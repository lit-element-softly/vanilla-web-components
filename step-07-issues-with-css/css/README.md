# Spectre.css

## Update style sheets

```shell
curl https://raw.githubusercontent.com/picturepan2/spectre/master/dist/spectre.css --output spectre.css
curl https://raw.githubusercontent.com/picturepan2/spectre/master/dist/spectre-exp.css --output spectre-exp.css
curl https://raw.githubusercontent.com/picturepan2/icons.css/master/dist/icons.css --output icons.css
```