class MyCounterTag extends HTMLElement {
  
  connectedCallback() {
    this.counter = 0
    this.attachShadow({ mode: 'open' });
    //this.shadowRoot.adoptedStyleSheets = [window.spectreCss, window.iconsCss]
    this.shadowRoot.adoptedStyleSheets = [window.k33gCss]
    
    this.render()
  }
  
  onClick() {
    this.counter ++
    this.render()
  }
  
  render() {
    return this.html(`
          <div class="container">
            <h2>${this.getAttribute("title")}: ${this.counter}</h2>
            <button>👋 Click Me</button>
            <p>🙏 Click on the button</p>
          </div>
        `)
  }
  
  html(content) {
    this.shadowRoot.innerHTML = content
    this.shadowRoot.querySelector('button').addEventListener('click', event => this.onClick())
  }
}


customElements.define('my-counter-tag', MyCounterTag)